const CANVAS_WIDTH = 1200, CANVAS_HEIGHT = 900;

function drawCurrentPos() {
    PGC.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    PGC.beginPath();
    transformCanvas(1, TR_GC);
    transformCanvas(1, PGC);
    PGC.strokeStyle = "red";
    PGC.lineWidth = 10;
    drawLineBetweenTwoPoints(0, 0, mass1X, mass1Y);
    drawBall(0, 0, 2);
    drawLineBetweenTwoPoints(mass1X, mass1Y, mass2X, mass2Y);
    drawTraces(traces);
    drawTraceNewCanvas();
    drawBall(mass1X, mass1Y, m1);
    drawBall(mass2X, mass2Y, m2);
    transformCanvas(-1, PGC);
    transformCanvas(-1, TR_GC);
}

function drawTraces(traces) {
    PGC.strokeStyle = "darkblue";
    PGC.lineWidth = 0.5;
    PGC.beginPath();
    if (traces.length >= 2) {
        for (let i = 0; i < traces.length - 1; i++) {
            PGC.moveTo(traces[i].mass2X, traces[i].mass2Y);
            PGC.lineTo(traces[i + 1].mass2X, traces[i + 1].mass2Y);
            PGC.stroke();
        }
        PGC.beginPath();
        for (let i = 0; i < traces.length - 1; i++) {
            PGC.moveTo(traces[i].mass1X, traces[i].mass1Y);
            PGC.lineTo(traces[i + 1].mass1X, traces[i + 1].mass1Y);
            PGC.stroke();
        }
    }
    if (traces.length > 300) traces.shift();
}

let prevX = mass2X, prevY = mass2Y;

function drawTraceNewCanvas() {
    TR_GC.strokeStyle = "darkblue";
    TR_GC.lineWidth = 0.5;

    TR_GC.moveTo(prevX, prevY);
    TR_GC.lineTo(mass2X, mass2Y);
    TR_GC.stroke();
    prevX = mass2X;
    prevY = mass2Y;
}

function transformCanvas(dir, context) {
    const x = CANVAS_WIDTH * dir / 2, y = CANVAS_HEIGHT * dir / 3, angle = Math.PI * dir / 2;
    if (dir === 1) {
        context.translate(x, y);
        context.rotate(angle);
    } else if (dir === -1) {
        context.rotate(angle);
        context.translate(x, y);
    }
}

function drawLineBetweenTwoPoints(x0, y0, x1, y1) {
    PGC.beginPath();
    PGC.moveTo(x0, y0);
    PGC.lineTo(x1, y1);
    PGC.stroke();
}

function drawBall(x, y, m) {
    let radius = BALL_R_FACTOR * Math.pow(m, .3333);
    PGC.beginPath();
    PGC.ellipse(x, y, radius, radius, 2 * Math.PI, 0, 2 * Math.PI);
    PGC.fill();
}

function setInitParams() {
    TR_GC.beginPath();
    PGC.fillStyle = "#000000"
}
