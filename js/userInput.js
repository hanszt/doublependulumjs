const PENDULUM_1_ANGLE_SLIDER = document.getElementById("pendulum1Angle");
const PENDULUM_2_ANGLE_SLIDER = document.getElementById("pendulum2Angle");
const PENDULUM_1_ANGLE_V_SLIDER = document.getElementById("pendulum1AngleV");
const PENDULUM_2_ANGLE_V_SLIDER = document.getElementById("pendulum2AngleV");
const PENDULUM_1_LENGTH_SLIDER = document.getElementById("pendulum1Length");
const PENDULUM_2_LENGTH_SLIDER = document.getElementById("pendulum2Length");
const PENDULUM_1_MASS_SLIDER = document.getElementById("mass1Slider");
const PENDULUM_2_MASS_SLIDER = document.getElementById("mass2Slider");
const FRICTION_SLIDER = document.getElementById("frictionSlider");
const ZOOM_SLIDER = document.getElementById("zoomSlider");

const ANGLE_1_LABEL = document.getElementById("angle_pendulum_1");
const ANGLE_2_LABEL = document.getElementById("angle_pendulum_2");
const ANGLE_V_1_LABEL = document.getElementById("angle_v_pendulum_1");
const ANGLE_V_2_LABEL = document.getElementById("angle_v_pendulum_2");
const LENGTH_1_LABEL = document.getElementById("length_pendulum_1");
const LENGTH_2_LABEL = document.getElementById("length_pendulum_2");
const M1_OUTPUT_LABEL = document.getElementById("value_mass_1");
const M2_OUTPUT_LABEL = document.getElementById("value_mass_2");
const FRICTION_OUTPUT_LABEL = document.getElementById("friction");
const ZOOM_OUTPUT_LABEL = document.getElementById("zoom");

ANGLE_1_LABEL.innerHTML = PENDULUM_1_ANGLE_SLIDER.value;
ANGLE_2_LABEL.innerHTML = PENDULUM_2_ANGLE_SLIDER.value;
ANGLE_V_1_LABEL.innerHTML = PENDULUM_1_ANGLE_V_SLIDER.value;
ANGLE_V_2_LABEL.innerHTML = PENDULUM_2_ANGLE_V_SLIDER.value;
LENGTH_1_LABEL.innerHTML = PENDULUM_1_LENGTH_SLIDER.value;
LENGTH_2_LABEL.innerHTML = PENDULUM_2_LENGTH_SLIDER.value;
M1_OUTPUT_LABEL.innerHTML = PENDULUM_1_MASS_SLIDER.value;
M2_OUTPUT_LABEL.innerHTML = PENDULUM_2_MASS_SLIDER.value;
FRICTION_OUTPUT_LABEL.innerHTML = FRICTION_SLIDER.value;
ZOOM_OUTPUT_LABEL.innerHTML = ZOOM_SLIDER.value;

PENDULUM_1_ANGLE_SLIDER.oninput = function () {
    angle1 = parseFloat(this.value) / 100;
    ANGLE_1_LABEL.innerHTML = angle1;
}

PENDULUM_2_ANGLE_SLIDER.oninput = function () {
    angle2 = parseFloat(this.value) / 100;
    ANGLE_2_LABEL.innerHTML = angle2;
}

PENDULUM_1_ANGLE_V_SLIDER.oninput = function () {
    vAngular1 = parseFloat(this.value) / 1000;
    ANGLE_1_LABEL.innerHTML = vAngular1;
}

PENDULUM_2_ANGLE_V_SLIDER.oninput = function () {
    vAngular2 = parseFloat(this.value) / 1000;
    ANGLE_2_LABEL.innerHTML = vAngular2;
}

PENDULUM_1_LENGTH_SLIDER.oninput = function () {
    pendulum1Length = parseInt(this.value);
    LENGTH_1_LABEL.innerHTML = pendulum1Length;
}

PENDULUM_2_LENGTH_SLIDER.oninput = function () {
    pendulum2Length = parseInt(this.value);
    LENGTH_2_LABEL.innerHTML = pendulum2Length;
}

PENDULUM_1_MASS_SLIDER.oninput = function () {
    m1 = parseInt(this.value);
    M1_OUTPUT_LABEL.innerHTML = m1;
}

PENDULUM_2_MASS_SLIDER.oninput = function () {
    m2 = parseInt(this.value);
    M2_OUTPUT_LABEL.innerHTML = m2;
}

FRICTION_SLIDER.oninput = function () {
    friction = parseFloat(this.value) / 10;
    FRICTION_OUTPUT_LABEL.innerHTML = friction;
}

ZOOM_SLIDER.oninput = function () {
    zoom = parseFloat(this.value);
    ZOOM_OUTPUT_LABEL.innerHTML = zoom;
}