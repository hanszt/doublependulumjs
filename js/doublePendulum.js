const PENDULUM_CANVAS = document.getElementById("pendulumCanvas");
const TRACE_CANVAS = document.getElementById("traceCanvas");
const PGC = PENDULUM_CANVAS.getContext("2d");
const TR_GC = TRACE_CANVAS.getContext("2d");

const TIME_OUT = 30;
const G = 9.81, BALL_R_FACTOR = 5;

let pendulum1Length = 200, pendulum2Length = 300, m1 = 50, m2 = 70, friction = 2, zoom = 1;

let angle1 = Math.PI * 2 / 3, angle2 = Math.PI / 3, vAngular1 = 0, vAngular2 = 0, aAngular1 = 0, aAngular2 = 0;
let mass1X = Math.cos(angle1) * pendulum1Length, mass1Y = Math.sin(angle1) * pendulum1Length,
    mass2X = mass1X + Math.cos(angle2) * pendulum2Length, mass2Y = mass1Y + Math.sin(angle2) * pendulum2Length;
const traces = [];

console.log("This is a simulation of a double pendulum motion.");
// console.log(pendulum1Length);
// console.log(pendulum2Length);
// console.log(m1);
// console.log(m2);
// console.log(angle1);
// console.log(angle2);

function calculateAngularAccM1() {
    let
        num1 = -G * (2 * m1 + m2) * Math.sin(angle1),
        num2 = -m2 * G * Math.sin(angle1 - 2 * angle2),
        num3 = -2 * Math.sin(angle1 - angle2) * m2,
        num4 = vAngular2 * vAngular2 * pendulum2Length + vAngular1 * vAngular1 * pendulum1Length * Math.cos(angle1 - angle2),
        den = pendulum1Length * (2 * m1 * m2 - m2 * Math.cos(2 * angle1 - 2 * angle2));
    aAngular1 = (num1 + num2 + num3 * num4) / den;

}

function calculateAngularAccM2() {
    let
        num1 = 2 * Math.sin(angle1 - angle2),
        num2 = vAngular1 * vAngular1 * pendulum1Length * (m1 + m2),
        num3 = G * (m1 + m2) * Math.cos(angle1),
        num4 = vAngular2 * vAngular2 * pendulum2Length * m2 * Math.cos(angle1 - angle2),
        den = pendulum2Length * (2 * m1 * m2 - m2 * Math.cos(2 * angle1 - 2 * angle2));
    aAngular2 = (num1 * (num2 + num3 + num4)) / den;
}

function calculateAngleVars() {
    angle1 += vAngular1 += aAngular1;
    angle2 += vAngular2 += aAngular2;
    vAngular1 *= (1 - (friction / 1000));
    vAngular2 *= (1 - (friction / 1000));
}

function drawPendulum() {
    calculateParams();
    drawCurrentPos();
}

function updateCoordinates() {
    mass1X = Math.cos(angle1) * pendulum1Length;
    mass1Y = Math.sin(angle1) * pendulum1Length;
    let tempX = Math.cos(angle2) * pendulum2Length, tempY = Math.sin(angle2) * pendulum2Length;
    mass2X = mass1X + tempX;
    mass2Y = mass1Y + tempY;
    traces.push({mass1X, mass1Y, mass2X, mass2Y});
    // console.log(`mass2X: ${mass2X}\n mass2Y: ${mass2Y}`);
}

function calculateParams() {
    calculateAngularAccM1();
    calculateAngularAccM2();
    calculateAngleVars();
    updateCoordinates();
    angle1 = setBoundToAngle(angle1);
    angle2 = setBoundToAngle(angle2);
    updateSlider(PENDULUM_1_ANGLE_SLIDER, ANGLE_1_LABEL,angle1);
    updateSlider(PENDULUM_2_ANGLE_SLIDER, ANGLE_2_LABEL, angle2);
    updateVelocitySlider(PENDULUM_1_ANGLE_V_SLIDER, ANGLE_V_1_LABEL,vAngular1);
    updateVelocitySlider(PENDULUM_2_ANGLE_V_SLIDER, ANGLE_V_2_LABEL, vAngular2);
}

function updateSlider(slider, label, value) {
    slider.value = value * 100;
    label.innerHTML = value.toFixed(2);
}

function updateVelocitySlider(slider, label, value) {
    slider.value = value * 1000;
    label.innerHTML = (value * TIME_OUT).toFixed(2);
}

function setBoundToAngle(angle) {
    if (angle > Math.PI) angle = angle - 2 * Math.PI;
    if (angle < -Math.PI) angle = angle + 2 * Math.PI;
    return angle;
}

setInterval(drawPendulum, TIME_OUT);